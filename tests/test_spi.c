#include <stdio.h>

#include "arch/stm32l4xxx/peripherals/rcc.h"
#include "arch/stm32l4xxx/peripherals/spi.h"
#include "kern/common.h"
#include "kern/spi/spi_manager.h"
#include "test_harness.h"

TEST(spi, smoke)
{
  int ec = 1;
  spi_opts_t opts = DEFAULT_SPI_OPTS;
  spi_t* spi = reserve_spi(SPI_SELECT_SPI1, &opts, &ec);

  ASSERT_EQ(ec, 0);
  ASSERT_TRUE(spi != NULL);

  uint32_t reg = 0;
  regset(reg, spi_bidimode, 0);
  regset(reg, spi_crcen, 0);
  regset(reg, spi_crcnext, 0);
  regset(reg, spi_crcl, 0);
  regset(reg, spi_rxonly, 0);
  regset(reg, spi_ssm, 1);
  regset(reg, spi_ssi, 1);
  regset(reg, spi_lsbfirst, 1);
  regset(reg, spi_spe, 1);
  regset(reg, spi_br, SPI_BAUD_FPCLK_DIV_32);
  regset(reg, spi_mstr, 1);
  regset(reg, spi_cpol, 0);
  regset(reg, spi_cpha, 0);

  ASSERT_EQ(SPI1.c_r1, reg);

  reg = 0;
  regset(reg, spi_ldma_tx, 0);
  regset(reg, spi_ldma_rx, 0);
  regset(reg, spi_frxth, 0);
  regset(reg, spi_ds, SPI_DATA_SIZE_8_BITS);
  regset(reg, spi_txeie, 0);
  regset(reg, spi_rxneie, 0);
  regset(reg, spi_errie, 0);
  regset(reg, spi_frf, 0);
  regset(reg, spi_nssp, 0);
  regset(reg, spi_ssoe, 0);
  regset(reg, spi_txdmaen, 0);
  regset(reg, spi_rxdmaen, 0);
  SPI1.c_r2 = reg;

  ASSERT_EQ(SPI1.c_r2, reg);

  ASSERT_TRUE(regget(RCC.apb2en_r, rcc_spi1en));

  return 0;
}

TEST(spi, double_reserve)
{
  int ec = 10;

  spi_opts_t opts = DEFAULT_SPI_OPTS;
  reserve_spi(SPI_SELECT_SPI1, &opts, &ec);
  ASSERT_EQ(ec, 0);
  spi_t* spi1_2 = reserve_spi(SPI_SELECT_SPI1, &opts, &ec);
  ASSERT_EQ(spi1_2, NULL);
  ASSERT_EQ(ec, SPI_ERROR_ALREADY_IN_USE);

  return 0;
}

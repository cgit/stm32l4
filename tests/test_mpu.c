#include "arch/arm/cortex-m4/mpu.h"
#include "kern/mpu/mpu_manager.h"
#include "test_harness.c"

TEST(mpu, smell)
{
  memory_region_opts_t memopts = {0};
  memopts.region = (void*)0;
  memopts.bufferable = 0;
  memopts.cacheable = 1;
  memopts.sharable = 1;
  memopts.tex = 0;
  memopts.size = REGION_SIZE_4Gb;
  memopts.perms = ACCESS_PERMS_FULL;
  memopts.subregion_disable = 0;
  memopts.executable = 1;
  memopts.enable = 1;

  mpu_configure_region(0, &memopts);
  mpu_set_enabled(1);

  ASSERT_EQ(MPU.rba_r, 1 << 4);
  ASSERT_EQ(MPU.ras_r, 0x0306003F);

  return 0;
}

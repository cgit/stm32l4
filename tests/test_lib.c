#include "kern/lib.h"
#include "test_harness.h"

TEST(lib, hexify)
{
  char buf[10];

  hexify(0xaaaaaaaa, buf);
  ASSERT_EQ_STR(buf, "aaaaaaaa");

  hexify(0xdddddddd, buf);
  ASSERT_EQ_STR(buf, "dddddddd");

  hexify(0x02468ace, buf);
  ASSERT_EQ_STR(buf, "02468ace");

  hexify(0xdeadbeef, buf);
  ASSERT_EQ_STR(buf, "deadbeef");

  return 0;
}

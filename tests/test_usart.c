#include <stdlib.h>

#include "arch/stm32l4xxx/peripherals/usart.h"
#include "test_harness.h"

TEST(usart, enable_dma)
{
  __IO usart_t* usart = &USART1;

  usart->c_r3 = 0;

  usart_enable_dma(usart, USART_ENABLE_TX);
  ASSERT_EQ(usart->c_r3, 128);

  usart_enable_dma(usart, USART_ENABLE_RX);
  ASSERT_EQ(usart->c_r3, 192);

  usart_enable_dma(usart, USART_ENABLE_DISABLED);
  ASSERT_EQ(usart->c_r3, 0);

  return 0;
}

#include "kern/mem.h"
#include "shared/array_list.h"
#include "test_harness.h"

ARRAY_LIST_DECL(int);
ARRAY_LIST_IMPL(int);

TEST(array_list, smell)
{
  array_list_t(int)* list = array_list_new(int)();

  array_list_add(int)(list, 1);
  array_list_add(int)(list, 2);
  array_list_add(int)(list, 3);
  array_list_add(int)(list, 4);
  array_list_add(int)(list, 5);

  int expected[5] = {1, 2, 3, 4, 5};

  for (size_t i = 0; i < 5; ++i) {
    ASSERT_EQ(*array_list_ref(int)(list, i), expected[i]);
  }

  ASSERT_EQ(array_list_length(int)(list), 5);
  *array_list_ref(int)(list, 3) = 8;
  expected[3] = 8;

  for (size_t i = 0; i < 5; ++i) {
    ASSERT_EQ(*array_list_ref(int)(list, i), expected[i]);
  }

  int out;
  array_list_remove(int)(list, 2, &out);
  ASSERT_EQ(out, 3);
  ASSERT_EQ(array_list_length(int)(list), 4);

  int expected2[4] = {1, 2, 8, 5};
  for (size_t i = 0; i < 4; ++i) {
    ASSERT_EQ(*array_list_ref(int)(list, i), expected2[i]);
  }

  return 0;
}

TEST(array_list, foreach)
{
  array_list_t(int)* arl = array_list_new(int)();
  array_list_add(int)(arl, 3);
  array_list_add(int)(arl, 2);
  array_list_add(int)(arl, 1);

  int i = 0;
  int values[3];
  array_list_foreach(arl, val)
  {
    values[i] = val;
    ++i;
  }

  ASSERT_EQ(values[0], 3);
  ASSERT_EQ(values[1], 2);
  ASSERT_EQ(values[2], 1);

  return 0;
}

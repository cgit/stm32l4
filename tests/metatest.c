#include "test_harness.h"

/* Tests the test harness itself. */

static int my_variable;
static int my_initialized_variable = 5;

TEST(meta, clobbers_variables)
{
  my_variable = 6;
  my_initialized_variable = 5;

  return 0;
}

TEST(meta, variables_reset)
{
  ASSERT_EQ(my_variable, 0);
  ASSERT_EQ(my_initialized_variable, 5);

  return 0;
}

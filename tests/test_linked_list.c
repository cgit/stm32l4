#include "kern/mem.h"
#include "shared/linked_list.h"
#include "test_harness.h"

LINKED_LIST_STATIC_DECL(int);
LINKED_LIST_STATIC_IMPL(int);

TEST(linked_list, smell)
{
  linked_list_t(int) ll = LINKED_LIST_INIT;

  ASSERT_EQ(linked_list_length(int)(&ll), 0);

  linked_list_push_front(int)(&ll, 5);
  ASSERT_EQ(linked_list_length(int)(&ll), 1);

  linked_list_push_front(int)(&ll, 3);
  linked_list_push_back(int)(&ll, 2);

  ASSERT_EQ(linked_list_length(int)(&ll), 3);
  ASSERT_EQ(*linked_list_front(int)(&ll), 3);
  ASSERT_EQ(*linked_list_back(int)(&ll), 2);

  linked_list_push_back(int)(&ll, 7);
  ASSERT_EQ(*linked_list_back(int)(&ll), 7);
  linked_list_pop_back(int)(&ll);
  ASSERT_EQ(*linked_list_back(int)(&ll), 2);

  linked_list_pop_front(int)(&ll);

  ASSERT_EQ(*linked_list_front(int)(&ll), 5);

  linked_list_pop_front(int)(&ll);

  ASSERT_EQ(*linked_list_front(int)(&ll), 2);

  linked_list_pop_front(int)(&ll);

  ASSERT_EQ(linked_list_front(int)(&ll), NULL);
  return 0;
}

TEST(linked_list, foreach)
{
  linked_list_t(int) ll = LINKED_LIST_INIT;
  linked_list_push_front(int)(&ll, 3);
  linked_list_push_front(int)(&ll, 2);
  linked_list_push_front(int)(&ll, 1);

  int i = 0;
  int values[3];
  linked_list_foreach(ll, val)
  {
    values[i] = val;
    ++i;
  }

  ASSERT_EQ(values[0], 1);
  ASSERT_EQ(values[1], 2);
  ASSERT_EQ(values[2], 3);

  return 0;
}

TEST(linked_list, remove)
{
  linked_list_t(int) ll = LINKED_LIST_INIT;
  linked_list_push_front(int)(&ll, 1);
  linked_list_push_front(int)(&ll, 2);
  linked_list_push_front(int)(&ll, 3);
  linked_list_push_front(int)(&ll, 4);
  linked_list_push_front(int)(&ll, 4);

  int ec = linked_list_remove(int)(&ll, 4);
  ASSERT_EQ(!!ec, 1);
  ec = linked_list_remove(int)(&ll, 2);
  ASSERT_EQ(!!ec, 1);
  ec = linked_list_remove(int)(&ll, 1);
  ASSERT_EQ(!!ec, 1);

  ASSERT_EQ(linked_list_length(int)(&ll), 1);
  ASSERT_EQ(*linked_list_front(int)(&ll), 3);

  return 0;
}

#include "arch/stm32l4xxx/peripherals/irq.h"
#include "arch/stm32l4xxx/peripherals/nvic.h"
#include "test_harness.h"

TEST(irq, nvic)
{
  interrupt_set_t is = {0};

  interrupt_set_add(&is, IRQ_USART2);
  interrupt_set_add(&is, IRQ_USART3);

  enable_interrupts(&is);

  ASSERT_EQ(is.irqs[1], 0xC0);
  ASSERT_EQ(NVIC.ise_r[1], 0xC0);

  return 0;
}

TEST(irq, nvic_edgecase)
{
  interrupt_set_t is = {0};
  interrupt_set_add(&is, IRQ_WWDG_IRQ);
  interrupt_set_add(&is, IRQ_I2C1_ER);

  enable_interrupts(&is);

  ASSERT_EQ(is.irqs[0], 1);
  ASSERT_EQ(NVIC.ise_r[0], 1);
  ASSERT_EQ(is.irqs[1], 1);
  ASSERT_EQ(NVIC.ise_r[1], 1);

  return 0;
}

TEST(irq, enable_single_interrupt)
{
  enable_interrupt(IRQ_USART2);
  ASSERT_EQ(NVIC.ise_r[1], 0x40);

  return 0;
}

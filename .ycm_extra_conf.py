import os
import ycm_core

flags = [
  '-Wall',
  '-DFOR_TESTING',
  '-Iinclude',
  '-Iinclude/arch/arm',
  '-Itest_harness/',
  '-DARCH_STMT32L4',
  '-DDRY_RUN',
  # -Iinclude/arch/arm -Itest_harness/ -DARCH_STM32L4 -DDRY_RUN -Wall -DFOR_TESTINGk
  ]

SOURCE_EXTENSIONS = [ '.cpp', '.cxx', '.cc', '.c', ]

def FlagsForFile( filename, **kwargs ):
  return {
  'flags': flags,
  'do_cache': True
  }

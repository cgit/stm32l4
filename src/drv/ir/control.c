#include "drv/ir/control.h"

#include "drv/ir/ir.h"
#include "shared/map.h"

typedef struct {
  void* closure;
  void (*fn)(uint32_t code, void* closure);
} ir_code_listener_t;

#define integer_cmp_(a, b) (a - b)
MAP_DECL(uint32_t, ir_code_listener_t);
MAP_IMPL(uint32_t, ir_code_listener_t, integer_cmp_, null_dtor);

static map_t(uint32_t, ir_code_listener_t) * listeners;

static void ir_callback(const ir_code_t* ir)
{
  uint32_t code;
  if (ir_generic_decode(ir, &code)) {
    ir_code_listener_t* l =
        map_get(uint32_t, ir_code_listener_t)(listeners, code);

    if (l) {
      l->fn(code, l->closure);
    }
  }
}

void add_ir_code_callback_(
    uint32_t code, void (*fn)(uint32_t, void*), void* closure)
{
  ir_code_listener_t l;
  l.fn = fn;
  l.closure = closure;
  map_put(uint32_t, ir_code_listener_t)(listeners, code, l);
}

void enable_ir_control()
{
  listeners = map_new(uint32_t, ir_code_listener_t)();
  add_ir_callback(ir_callback);
}

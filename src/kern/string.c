#include "kern/string.h"

void kstrcpy(char* into, const char* from)
{
  while (*from) {
    *(into++) = *(from++);
  }
  *into = 0;
}

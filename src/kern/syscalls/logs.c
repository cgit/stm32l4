#include "kern/log.h"
#include "kern/syscall.h"

void kern_logs(const char* str)
{
  klogf("[Log] - %s", str);
}

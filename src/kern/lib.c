#include "kern/lib.h"

#define nybble_to_hex(n) ((n) < 10 ? 0x30 + (n) : ('a' + ((n)-10)))

void hexify(uint32_t v, char* into)
{
  into += 8;

  *(into--) = 0;

  *(into--) = nybble_to_hex(v & 0x0F);
  v >>= 4;
  *(into--) = nybble_to_hex(v & 0x0F);
  v >>= 4;
  *(into--) = nybble_to_hex(v & 0x0F);
  v >>= 4;
  *(into--) = nybble_to_hex(v & 0x0F);
  v >>= 4;

  *(into--) = nybble_to_hex(v & 0x0F);
  v >>= 4;
  *(into--) = nybble_to_hex(v & 0x0F);
  v >>= 4;
  *(into--) = nybble_to_hex(v & 0x0F);
  v >>= 4;
  *into = nybble_to_hex(v & 0x0F);
  v >>= 4;
}

void decimalify(int v, char* into)
{
  int c = 0;
  int i;

  if (v == 0) {
    *(into++) = '0';
    *into = 0;
    return;
  } else {
    if (v < 0) {
      v = -v;
      *(into++) = '-';
    }

    while (v > 0) {
      *(into++) = 0x30 + (v % 10);
      v /= 10;
      ++c;
    }
  }
  *into = 0;

  into -= c;
  for (i = 0; i < c / 2; ++i) {
    char tmp = into[i];
    into[i] = into[c - i - 1];
    into[c - i - 1] = tmp;
  }
}

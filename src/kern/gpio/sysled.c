#include "kern/gpio/sysled.h"

#define SYSLED GPIO_PIN_PB3

gpio_reserved_pin_t get_sysled()
{
  if (gpio_pin_in_use(SYSLED)) {
    return (gpio_reserved_pin_t){.v_ = SYSLED};
  }

  int ec;
  gpio_pin_opts_t opts = DEFAULT_GPIO_OPTS_OUTPUT;
  return reserve_gpio_pin(SYSLED, &opts, &ec);
}

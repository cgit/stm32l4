#include "arch.h"
#include "kern/panic.h"
/*
 * Replacement for common stdlib functions that don't exist
 * on the ARM bare-metal compilation environment.
 */

#include <stddef.h>

size_t strlen(char* ch)
{
  size_t ret = 0;
  while (*(ch++) != 0) ++ret;
  return ret;
}

#ifdef ARCH_STM32L4

void* memcpy(void* dest, const void* src, size_t size)
{
  uint8_t* dest_ = dest;
  const uint8_t* src_ = src;

  while (size--) {
    *(dest_++) = *(src_++);
  }

  return dest;
}

int memcmp(const void* s1_, const void* s2_, size_t size)
{
  const uint8_t* s1 = s1_;
  const uint8_t* s2 = s2_;

  while (size--) {
    if (*s1 != *s2) {
      if (*s1 > *s2) {
        return 1;
      } else {
        return -1;
      }
    }

    ++s1;
    ++s2;
  }

  return 0;
}

void __assert_func(
    const char* file, int line, const char* func, const char* failedexpr)
{
  panic("Assertion failed: %s:%d in %s at %s\n", file, line, func, failedexpr);
}

#endif

#include "user/syscall.h"

#include <stdint.h>

#include "arch.h"

void __attribute__((noinline))
do_syscall(volatile uint32_t id, volatile uint32_t arg)
{
#ifdef ARCH_STM32L4
  asm volatile("svc #0x04");
#endif
}

#define SYSCALL(id, fn, kernfn, argt) \
  void fn(argt arg) { do_syscall(id, (uint32_t)arg); }
#include "kern/syscall/syscall_tbl.inc"

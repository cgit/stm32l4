#include "kern/common.h"
#include "kern/log.h"
#include "user/syscall.h"

void usermode_start()
{
  logs("I'm in usermode!\n");
  logs("I'm still in usermode!\n");
  logs("I'm super still in usermode!\n");

  for (;;)
    ;
}

#include "arch/stm32l4xxx/peripherals/irq.h"

#include "arch.h"
#include "arch/stm32l4xxx/peripherals/gpio.h"
#include "arch/stm32l4xxx/peripherals/nvic.h"
#include "kern/delay.h"
#include "kern/gpio/gpio_manager.h"
#include "kern/init.h"
#include "kern/log.h"

#define IRQ_RESERVED(n)
#define IRQ(name, uname_, n) \
  void WEAK name() { unhandled_isr(n); }
#include "arch/stm32l4xxx/peripherals/isrs.inc"
#undef IRQ_RESERVED
#undef IRQ

void isr_simple_pin_on()
{
  int ec;
  gpio_pin_opts_t opts = DEFAULT_GPIO_OPTS_OUTPUT;
  gpio_reserved_pin_t pin3 = reserve_gpio_pin(GPIO_PIN_PB3, &opts, &ec);

  set_gpio_pin_high(pin3);
}

#ifdef ARCH_STM32L4
#define IRQ_RESERVED(n) 0,
#define IRQ(name, uname_, n) name,
const void* vectors[] __attribute__((section(".vectors"))) = {
    (void*)STACK_TOP, /* Top of stack at top of sram1. 48k */
#include "arch/stm32l4xxx/peripherals/isrs.inc"
};
#undef IRQ_RESERVED
#undef IRQ
#endif

/* Encodes the provided number as a series of flashes on the on-board
 * LED. The flashes follow as such:
 *
 * Before the bits of the code are flashed, a rapid succession of 20 flashes
 * followed by a pause will occur indicating that the next 8 flashes indicate
 * the bits of the provided code.
 *
 * Eoch of the next eight flashes indicate either a 1 or 0 depending on the
 * length of flash. The first flash is the least-significant bit, the next the
 * second least, the third third least, etc.
 *
 * - A quick flash followed by a long pause indicates a 0 bit.
 * - A "long" flash followed by a equally long pause indicates a 1 bit.
 */
void unhandled_isr(uint8_t number)
{
  if (get_system_init_level() > 2) {
    /* If we have access to logging, just log the unhandled interrupt
     * before contituing with the fancy flashing. */
    klogf("Unhandled ISR: %d\n", (int)number);
  }

  int ec;
  gpio_pin_opts_t opts = DEFAULT_GPIO_OPTS_OUTPUT;
  gpio_reserved_pin_t pin3 = reserve_gpio_pin(GPIO_PIN_PB3, &opts, &ec);

  for (;;) {
    for (int i = 0; i < 20; ++i) {
      set_gpio_pin_high(pin3);
      delay(1000000);
      set_gpio_pin_low(pin3);
      delay(1000000);
    }
    delay(50000000);

    int n = number;
    for (int i = 0; i < 8; ++i) {
      if (n & 1) {
        // LSB is a 1
        set_gpio_pin_high(pin3);
        delay(15000000);
        set_gpio_pin_low(pin3);
        delay(15000000);
      } else {
        // LSB is a 0
        set_gpio_pin_high(pin3);
        delay(1000000);
        set_gpio_pin_low(pin3);
        delay(29000000);
      }

      n >>= 1;
    }
  }
}

void enable_interrupts(interrupt_set_t* interrupts)
{
  for (int i = 0; i < sizeof(NVIC.ise_r) / sizeof(uint32_t); ++i)
    NVIC.ise_r[i] = interrupts->irqs[i];
}

void disable_interrupts(interrupt_set_t* interrupts)
{
  for (int i = 0; i < sizeof(NVIC.ise_r) / sizeof(uint32_t); ++i)
    NVIC.ice_r[i] = interrupts->irqs[i];
}

#ifndef WS2812B_H_
#define WS2812B_H_

#include "kern/common.h"
#include "kern/spi/spi_manager.h"

typedef struct {
  /* why is it out of order? Don't know! That's the way the hardware was
   * designed! */
  uint8_t g;
  uint8_t r;
  uint8_t b;
} PACKED rgb_t;

typedef struct {
  spi_t* spi;
} ws2812b_t;

static_assert(sizeof(rgb_t) == 3, "Sizeof rgb_t should be 3.");

ws2812b_t* ws2812b_new(spi_select_t spi_select, int* ec);

void ws2812b_latch(ws2812b_t* drv);

uint8_t* ws2812b_compile_rgb(rgb_t* out, size_t arr_len);

void ws2812b_write_rgb_sync(ws2812b_t* drv, uint8_t r, uint8_t g, uint8_t b);

void ws2812b_write_rgb_arr_sync(ws2812b_t* drv, rgb_t* arr, size_t arr_len);

void ws2812b_delete(ws2812b_t* drv);

#endif /* WS2812B_H_ */

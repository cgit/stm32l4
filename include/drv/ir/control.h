#ifndef _DRV_IR_CONTROL_H_
#define _DRV_IR_CONTROL_H_

#include "kern/common.h"

#define add_ir_code_callback(code, fn, closure) \
  add_ir_code_callback_(code, (void (*)(uint32_t, void*))(fn), closure)

void add_ir_code_callback_(
    uint32_t code, void (*fn)(uint32_t code, void* closure), void* closure);

void enable_ir_control();

#endif /* _DRV_IR_CONTROL_H_ */

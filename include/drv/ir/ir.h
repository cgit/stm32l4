#ifndef _DRV_IR_IR_H_
#define _DRV_IR_IR_H_

#include "kern/common.h"

typedef struct {
  uint16_t ts[64]; /* Timestamps. Microseconds. */
  uint8_t n;
} ir_code_t;

typedef struct {
  uint32_t c;
} decoded_ir;

/*
 * Attempt to decode the provided ir code.
 *
 * The generic ir is:
 *
 * The frame starts out as really-long, half(ish) as long, short. If
 * this basic frame header does not exist decoding cannot continue.
 *
 * A short code followed by a short code = 0
 * A long code followed by a short code = 1
 *
 * returns non-zero on successful decode.
 */
int ir_generic_decode(const ir_code_t* code, uint32_t* out);

void add_ir_callback(void(*callback)(const ir_code_t*));

void ir_begin_listen();

#endif /* _DRV_IR_IR_H_ */

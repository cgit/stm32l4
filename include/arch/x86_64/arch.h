#ifndef ARCH_H_
#define ARCH_H_

#include <stdint.h>
#include "fake_env.h"

#define ARCH_PC

#define enable_all_interrupts() do {} while(0)
#define disable_all_interrupts() do {} while(0)
#define __isb() do {} while(0)
#define __dsb() do {} while(0)

#define RCC_BASE (load_fake_rcc__())

#define DMA1_BASE (load_fake_ahb1__() + 0x0)
#define DMA2_BASE (load_fake_ahb1__() + 0x400)

#define USART1_BASE (load_fake_apb2__() + 0x3800)
#define USART2_BASE (load_fake_apb1__() + 0x4400)

#define GPIOA_BASE (load_fake_ahb2__() + 0x0)
#define GPIOB_BASE (load_fake_ahb2__() + 0x400)
#define GPIOC_BASE (load_fake_ahb2__() + 0x800)
#define GPIOH_BASE (load_fake_ahb2__() + 0x1C00)

#define SRAM1_BASE (load_fake_sram1__() + 0x0)
#define SRAM2_BASE (load_fake_sram2__() + 0x0)

#define SYSTEM_CONFIG_BLOCK_BASE (load_fake_scb__())
#define NVIC_BASE (load_fake_nvic__())

#define SPI1_BASE (load_fake_spi1__())
#define SPI3_BASE (load_fake_spi3__())

#define MPU_BASE (load_fake_mpu__())
#define SYSCFG_BASE (load_fake_syscfg__())
#define EXTI_BASE (load_fake_exti__())

#define TIM2_BASE (load_fake_exti__())

// Pretend there's a data segement at the start of SRAM1 for more accurate
// testing.
#define GHOST_DATA_SEGMENT_SIZE 1200
#define HEAP_START (*(((unsigned char*)SRAM1_BASE) +  GHOST_DATA_SEGMENT_SIZE))
#define HEAP_STOP (*(&HEAP_START + 49152 - GHOST_DATA_SEGMENT_SIZE))

#endif /* ARCH_H_ */

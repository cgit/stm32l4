#ifndef PERIPHERALS_SYSCFG_H_
#define PERIPHERALS_SYSCFG_H_

#include "arch.h"
#include "kern/common.h"

#define SYSCFG (*(syscfg_t*)SYSCFG_BASE)

typedef __IO struct {
#define syscfg_memmode (7 << 0) /* Memory mapping selection. */
#define syscfg_fbmode (1 << 8) /* Flash bank mode selection. */
  uint32_t memrmp_r; /* Configuration on memory map. */

#define syscfg_fwdis (1 << 0) /* Disable firewall. */
#define syscfg_boosten (1 << 8) /* Enable analog boost. */
#define syscfg_i2c4_fmp                                                              \
  (1 << 23) /* Fast-mode Plus driving capability activation (this bit is only \
               available on */
#define syscfg_i2c3_fmp \
  (1 << 22) /* I2C3 Fast-mode Plus driving capability activation */
#define syscfg_i2c2_fmp \
  (1 << 21) /* I2C2 Fast-mode Plus driving capability activation */
#define syscfg_i2c1_fmp \
  (1 << 20) /* I2C1 Fast-mode Plus driving capability activation */
#define syscfg_i2c_pb9_fmp \
  (1 << 19) /* Fast-mode Plus (Fm+) driving capability activation on PB9 */
#define syscfg_i2c_pb8_fmp \
  (1 << 18) /* Fast-mode Plus (Fm+) driving capability activation on PB8 */
#define syscfg_i2c_pb7_fmp \
  (1 << 17) /* Fast-mode Plus (Fm+) driving capability activation on PB7 */
#define syscfg_i2c_pb6_fmp \
  (1 << 16) /* Fast-mode Plus (Fm+) driving capability activation on PB6 */
#define syscfg_fpe_ie (0x3f << 26) /* Floating point interrupts enabled. */
  uint32_t cfg_r1; /* Configuration register 1. */

#define syscfg_exti0 (0xf << 0)
#define syscfg_exti1 (0xf << 4)
#define syscfg_exti2 (0xf << 8)
#define syscfg_exti3 (0xf << 12)
  uint32_t extic_r1; /* External interrupt configuration register 1. */

#define syscfg_exti4 (0xf << 0)
#define syscfg_exti5 (0xf << 4)
#define syscfg_exti6 (0xf << 8)
#define syscfg_exti7 (0xf << 12)
  uint32_t extic_r2; /* External interrupt configuration register 2. */

#define syscfg_exti8 (0xf << 0)
#define syscfg_exti9 (0xf << 4)
#define syscfg_exti10 (0xf << 8)
#define syscfg_exti11 (0xf << 12)
  uint32_t extic_r3; /* External interrupt configuration register 3. */

#define syscfg_exti12 (0xf << 0)
#define syscfg_exti13 (0xf << 4)
#define syscfg_exti14 (0xf << 8)
#define syscfg_exti15 (0xf << 12)
  uint32_t extic_r4; /* External interrupt configuration register 4. */

#define syscfg_sram2er (1 << 0)
#define syscfg_sram2bsy (1 << 1)
  uint32_t scs_r; /* SRAM2 control & status register. */

#define syscfg_spf (1 << 8) /* SRAM2 parity error flag */
#define syscfg_eccl (1 << 3) /* ECC Lock */
#define syscfg_pvdl (1 << 2) /* PVD lock enable bit */
#define syscfg_spl (1 << 1) /* SRAM2 parity lock bit */
#define syscfg_cll (1 << 0) /* Cortex-M4 LOCKUP (Hardfault) output enable bit */
  uint32_t cfg_r2; /* Configuration register 2. */

#define syscfg_pnwp(n) (1 << (n)) /* Page n write protection on sram2. */
  uint32_t swp_r1; /* SRAM2 software protection register 1. */

  union {
    uint8_t sk_r; /* SRAM2 write protection key for software erase. */
    uint32_t reserved;
  };

  uint32_t swp_r2; /* SRAM2 software protection register 2. */


} syscfg_t;

static_assert(
    offsetof(syscfg_t, swp_r2) == 0x28, "Offset check failed");

#endif /* PERIPHERALS_SYSCFG_H_ */

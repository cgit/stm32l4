#ifndef H__RCC_
#define H__RCC_

#include <stdint.h>

#include "arch.h"
#include "kern/common.h"

typedef struct {
  __IO uint32_t c_r;      /* Clock control register. 0x00 */
  __IO uint32_t icsc_r;   /* Internal clock srcs calibration register. 0x04 */
  __IO uint32_t cfg_r;    /* clock confguration register. 0x08 */
  __IO uint32_t pllcfg_r; /* PLL Configuration register. 0x0c */
  __IO uint32_t pllsai1cfg_r; /* PLLSAI1 configuration register. 0x10 */

  __IO uint32_t reserved_1; /* Not used. offset 0x14. */

  __IO uint32_t cie_r; /* Clock interrupt enable register. 0x18 */
  __IO uint32_t cif_r; /* Clock interrupt flag regiseter.  0x1c */
  __IO uint32_t cic_r; /* Clock interrupt clear register.  0x20 */

  __IO uint32_t reserved_2; /* Not used. offset 0x24. */

  __IO uint32_t ahb1rst_r; /* AHB Peripheral 1 reset register. 0x28 */
  __IO uint32_t ahb2rst_r; /* AHB Peripheral 2 reset register. 0x2c */
  __IO uint32_t ahb3rst_r; /* AHB Peripheral 3 reset register. 0x30 */

  __IO uint32_t reserved_3; /* Not used. offset 0x34. */

#define rcc_lptim1rst (1 << 31)  // Low Power Timer 1 reset
#define rcc_opamprst (1 << 30)  // OPAMP interface reset
#define rcc_dac1rst (1 << 29)  // DAC1 interface reset
#define rcc_pwrrst (1 << 28)  // Power interface reset
#define rcc_can2rst \
  (1 << 26)  // CAN2 reset (this bit is reserved for STM32L47x/L48x devices)
#define rcc_can1rst (1 << 25)  // CAN1 reset
#define rcc_crsrst \
  (1 << 24)  // CRS reset (this bit is reserved for STM32L47x/L48x devices)
#define rcc_i2c3rst (1 << 23)  // I2C3 reset
#define rcc_i2c2rst (1 << 22)  // I2C2 reset
#define rcc_i2c1rst (1 << 21)  // I2C1 reset
#define rcc_uart5rst (1 << 20)  // UART5 reset
#define rcc_uart4rst (1 << 19)  // UART4 reset
#define rcc_usart3rst (1 << 18)  // USART3 reset
#define rcc_usart2rst (1 << 17)  // USART2 reset
#define rcc_reserved (1 << 16)  // must be kept at reset value.
#define rcc_spi3rst (1 << 15)  // SPI3 reset
#define rcc_spi2rst (1 << 14)  // SPI2 reset
#define rcc_lcdrst \
  (1 << 9)  // interface reset (this bit is reserved for STM32L471/L4x5 devices)
#define rcc_tim7rst (1 << 5)  // timer reset
#define rcc_tim6rst (1 << 4)  // timer reset
#define rcc_tim5rst (1 << 3)  // timer reset
#define rcc_tim4rst (1 << 2)  // timer reset
#define rcc_tim3rst (1 << 1)  // timer reset
#define rcc_tim2rst (1 << 0)  // timer reset
  __IO uint32_t apb1rst1_r; /* APB Peripheral reset register 1. 0x38 */
  __IO uint32_t apb1rst2_r; /* APB Peripheral reset register 2. 0x3C */
  __IO uint32_t apb2rst_r;  /* APB Peripheral reset register.   0x40 */

  __IO uint32_t reserved_4; /* Not used. offset 0x44. */

#define rcc_dma1en (1 << 0) /* DMA1 clock enable. */
#define rcc_dma2en (1 << 1) /* DMA2 clock enable. */
#define rcc_flashen (1 << 8) /* Flash memory interface clock enable. */
#define rcc_crcen (1 << 12) /* CRC clock enable. */
#define rcc_tscen (1 << 16) /* Touch sensing controller clock enable. */
#define rcc_dmad2en (1 << 17) /* DMA2D clock enabled. */
  __IO uint32_t ahb1en_r; /* AHB1 Peripheral enable register. 0x48 */

#define rcc_gpioen(port) (1 << (port))
#define rcc_otgfsen (1 << 12)
#define rcc_adcen (1 << 13)
#define rcc_dcmien (1 << 14)
#define rcc_assen (1 << 16)
#define rcc_hashen (1 << 17)
#define rcc_rngen (1 << 18)
  __IO uint32_t ahb2en_r; /* AHB2 Peripheral enable register. 0x4C */
  __IO uint32_t ahb3en_r; /* AHB3 Peripheral enable register. 0x50 */

  __IO uint32_t reserved_5; /* Not used. offset 0x54. */

#define rcc_lptim1en (1 << 31) /* Low power timer 1 clock enable */
#define rcc_opampen (1 << 30) /* OPAMP interface clock enable */
#define rcc_dac1en (1 << 29) /* DAC1 interface clock enable */
#define rcc_pwren (1 << 28) /* Power interface clock enable */
#define rcc_can2en (1 << 26) /* CAN2 clock enable (this bit is reserved for STM32L47x/L48x devices) */
#define rcc_can1en (1 << 25) /* CAN1 clock enable */
#define rcc_crsen (1 << 24) /* Clock Recovery System clock enable (this bit is reserved for STM32L47x/L48x */
#define rcc_i2c3en (1 << 23) /* I2C3 clock enable */
#define rcc_i2c2en (1 << 22) /* I2C2 clock enable */
#define rcc_i2c1en (1 << 21) /* I2C1 clock enable */
#define rcc_uart5en (1 << 20) /* UART5 clock enable */
#define rcc_uart4en (1 << 19) /* UART4 clock enable */
#define rcc_usart3en (1 << 18) /* USART3 clock enable */
#define rcc_usart2en (1 << 17) /* USART2 clock enable */
#define rcc_spi3en (1 << 15) /* SPI3 clock enable */
#define rcc_spi2en (1 << 14) /* SPI2 clock enable */
#define rcc_wwdgen (1 << 11) /* Window watchdog clock enable */
#define rcc_rtcapben (1 << 10) /* RTC APB clock enable (this bit is reserved for STM32L47x/L48x devices) */
#define rcc_lcden (1 << 9) /* LCD clock enable (this bit is reserved for STM32L471/L4x5 devices) */
#define rcc_tim7en (1 << 5) /* TIM7 timer clock enable */
#define rcc_tim6en (1 << 4) /* TIM6 timer clock enable */
#define rcc_tim5en (1 << 3) /* TIM5 timer clock enable */
#define rcc_tim4en (1 << 2) /* TIM4 timer clock enable */
#define rcc_tim3en (1 << 1) /* TIM3 timer clock enable */
#define rcc_tim2en (1 << 0) /* TIM2 timer clock enable */
  __IO uint32_t apb1en1_r; /* APB1 Peripheral enable register 1. 0x58 */

#define lptim2en (1 << 5) /*Low power timer 2 clock enable */
#define swpmi1en (1 << 2) /* Single wire protocol clock enable */
#define i2c4en (1 << 1) /* I2C4 clock enable (this bit is reserved for STM32L47x/L48x devices) */
#define lpuart1en (1 << 0) /* Low power UART 1 clock enable */
  __IO uint32_t apb1en2_r; /* APB1 Peripheral enable register 2. 0x5C */

#define rcc_syscfgen (1 << 0)
#define rcc_fwen (1 << 7)
#define rcc_sdmmc1en (1 << 10)
#define rcc_tim1en (1 << 11)
#define rcc_spi1en (1 << 12)
#define rcc_tim8en (1 << 13)
#define rcc_usart1en (1 << 14)
#define rcc_tim15en (1 << 16)
#define rcc_tim16en (1 << 17)
#define rcc_tim17en (1 << 18)
#define rcc_sai1en (1 << 21)
#define rcc_sai2en (1 << 22)
#define rcc_dfsdm1en (1 << 24)
  __IO uint32_t apb2en_r; /* APB2 Peripheral enable register.   0x60 */

  __IO uint32_t reserved_6; /* Not used. offset 0x64. */

  __IO uint32_t ahb1smen_r; /* 0x68 */
  __IO uint32_t ahb2smen_r; /* 0x6c */
  __IO uint32_t ahb3smen_r; /* 0x70 */

  __IO uint32_t reserved_7;

  __IO uint32_t apb1smen_r1; /* 0x78 */
  __IO uint32_t apb1smen_r2; /* 0x7c */
  __IO uint32_t apb2smen_r;  /* 0x80 */

  __IO uint32_t reserved_8;

  __IO uint32_t ccip_r; /* 0x88 */
} PACKED rcc_t;

static_assert(offsetof(rcc_t, ccip_r) == 0x88, "Offset check failed.");

#define RCC (*(__IO rcc_t*)RCC_BASE)

/* Macros to operate on the RCC registers. */

/* Sets the HSE. rcc is the RCC to use, e is zero for off, non-zero for on. */
#define set_hse(rcc, e)        \
  do {                         \
    if (e) {                   \
      (rcc).c_r |= 1 << 16;    \
    } else {                   \
      (rcc).c_r &= ~(1 << 16); \
    }                          \
  } while (0)

/* Sets the HSI. rcc is the RCC to use, e is zero for off, non-zero for on. */
#define set_hsi(rcc, e)       \
  do {                        \
    if (e) {                  \
      (rcc).c_r |= 1 << 8;    \
    } else {                  \
      (rcc).c_r &= ~(1 << 8); \
    }                         \
  } while (0)

/* Checks to see if the hse is ready. */
#define hse_ready(rcc) ((rcc).c_r & (1 << 17))

/* Checks to see if the hse is ready. */
#define hsi_ready(rcc) ((rcc).c_r & (1 << 10))

#endif

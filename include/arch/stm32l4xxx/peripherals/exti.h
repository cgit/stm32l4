#ifndef PERIPHERALS_EXTI_H_
#define PERIPHERALS_EXTI_H_

#include "arch.h"

#define EXTI (*(exti_regs_t*)(EXTI_BASE))

typedef struct {
#define exti_im_n(n) (1 << (n))
  uint32_t im_r1; /* Interrupt mask register 1. */

#define exti_em_n(n) (1 << (n))
  uint32_t em_r1; /* Event mask register 1. */

#define exti_rt_n(n) (1 << (n))
  uint32_t rts_r1; /* Rising trigger selection register 1. */

#define exti_ft_n(n) (1 << (n))
  uint32_t fts_r1; /* Falling trigger selection register 1. */

#define exti_swi_n(n) (1 << (n))
  uint32_t swie_r1; /* Software interrupt event register 1. */

#define exti_pif_n(n) (1 << (n))
  uint32_t p_r1; /* Pending register 1. */

  uint32_t im_r2;   /* Interrupt mask register 2. */
  uint32_t em_r2;   /* Event mask register 2. */
  uint32_t rts_r2;  /* Rising trigger selection register 2. */
  uint32_t fts_r2;  /* Falling trigger selection register 2. */
  uint32_t swie_r2; /* Software interrupt event register 2. */
  uint32_t p_r2;    /* Pending register 2. */
} exti_regs_t;

#endif /* PERIPHERALS_EXTI_H_ */

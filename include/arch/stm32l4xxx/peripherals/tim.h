#ifndef ARCH_STM32L4XXX_PERIPHERALS_TIM_H_
#define ARCH_STM32L4XXX_PERIPHERALS_TIM_H_

#include "arch.h"
#include "kern/common.h"

#define TIM2 (*((tim_regs_t*)TIM2_BASE))

typedef __IO struct {
#define tim_uifremap (1 << 11) /* UIF status bit remapping */
#define tim_ckd (3 << 8) /* Clock Divisor */
#define tim_arpe (1 << 7) /* Auto-reload preload enable */
#define tim_cms (3 << 5) /* Center align mode selection */
#define tim_dir (1 << 4) /* Direction */
#define tim_opm (1 << 3) /* One-pulse mode */
#define tim_urs (1 << 2) /* Update request source */
#define tim_udis (1 << 1) /* Update disable */
#define tim_cen (1 << 0) /* Counter enable */
  uint32_t c_r1;

#define tim_ti1s (1 << 7)
#define tim_mms (7 << 4)
#define tim_ccds (1 << 3)
  uint32_t c_r2;

#define tim_sms_3 (1 << 16)
#define tim_etp (1 << 15)
#define tim_ece (1 << 14)
#define tim_etps (3 << 12)
#define tim_etf (0xF << 8)
#define tim_msm (1 << 7)
#define tim_ts (7 << 4)
#define tim_occs (1 << 3)
#define tim_sms_0_2 (7 << 0)
  uint32_t smc_r;

#define tim_tde (1 << 14) /* Trigger DMA request enable */
#define tim_cc4de (1 << 12) /* Capture/Compare 4 DMA request enable */
#define tim_cc3de (1 << 11) /* Capture/Compare 3 DMA request enable */
#define tim_cc2de (1 << 10) /* Capture/Compare 2 DMA request enable */
#define tim_cc1de (1 << 9) /* Capture/Compare 1 DMA request enable */
#define tim_ude (1 << 8) /* Update DMA request enable */
#define tim_tie (1 << 6) /* Trigger interrupt enable */
#define tim_cc4ie (1 << 4) /* Capture/Compare 4 interrupt enable */
#define tim_cc3ie (1 << 3) /* Capture/Compare 3 interrupt enable */
#define tim_cc2ie (1 << 2) /* Capture/Compare 2 interrupt enable */
#define tim_cc1ie (1 << 1) /* Capture/Compare 1 interrupt enable */
#define tim_uie (1 << 0) /* Update interrupt enable */
  uint32_t die_r; /* Dma interrupt/enable register */

#define tim_cc4of (1 << 12) /* Capture/Compare 4 overcapture flag */
#define tim_cc3of (1 << 11) /* Capture/Compare 3 overcapture flag */
#define tim_cc2of (1 << 10) /* Capture/compare 2 overcapture flag */
#define tim_cc1of (1 << 9) /* Capture/Compare 1 overcapture flag */
#define tim_tif (1 << 6) /* Trigger interrupt flag */
#define tim_cc4if (1 << 4) /* Capture/Compare 4 interrupt flag */
#define tim_cc3if (1 << 3) /* Capture/Compare 3 interrupt flag */
#define tim_cc2if (1 << 2) /* Capture/Compare 2 interrupt flag */
#define tim_cc1if (1 << 1) /* Capture/compare 1 interrupt flag */
#define tim_uif (1 << 0) /* Update interrupt flag */
  uint32_t s_r; /* Status register */

#define tim_tg (1 << 6) /* Trigger generation */
#define tim_cc4g (1 << 4) /* Capture/compare 4 generation */
#define tim_cc3g (1 << 3) /* Capture/compare 3 generation */
#define tim_cc2g (1 << 2) /* Capture/compare 2 generation */
#define tim_cc1g (1 << 1) /* Capture/compare 1 generation */
#define tim_ug (1 << 0) /* Update generation */
  uint32_t eg_r; /* Event generation register. */

#define tim_ic2f (15 << 12) /* Input capture 2 filter */
#define tim_ic2psc (3 << 10) /* Input capture 2 prescaler */
#define tim_cc2s (3 << 8) /* Capture/compare 2 selection */
#define tim_ic1f (15 << 4) /* Input capture 1 filter */
#define tim_ic1psc (3 << 2) /* Input capture 1 prescaler */
#define tim_cc1s (3 << 0) /* Capture/Compare 1 selection */
  /* Alternate */
#define tim_oc2ce (1 << 15) /* Output compare 2 clear enable */
#define tim_oc2m_3 (1 << 24) /* Output compare 2 mode, bit 3. */
#define tim_oc2m_0_2 (7 << 12) /* Output compare 2 mode, bits 0-2 */
#define tim_oc2pe (1 << 11) /* Output compare 2 preload enable */
#define tim_oc2fe (1 << 10) /* Output compare 2 fast enable */
#define tim_cc2s (3 << 8) /* Capture/Compare 2 selection */
#define tim_oc1ce (1 << 7) /* Output compare 1 clear enable */
#define tim_oc1m_3 (1 << 16) /* Output compare 1 mode, bit 3. */
#define tim_oc1m_0_2 (7 << 4) /* Output compare 1 mode, bits 0-2. */
#define tim_oc1pe (1 << 3) /* Output compare 1 preload enable */
#define tim_oc1fe (1 << 2) /* Output compare 1 fast enable */
#define tim_cc1s (3 << 0) /* Capture/Compare 1 selection */
  uint32_t ccm_r1; /* Capture/compare mode register 1. */

#define tim_ic4f (15 << 12) /* Input capture 4 filter */
#define tim_ic4psc (3 << 10) /* Input capture 4 prescaler */
#define tim_cc4s (3 << 8) /* Capture/Compare 4 selection */
#define tim_ic3f (15 << 4) /* Input capture 3 filter */
#define tim_ic3psc (3 << 2) /* Input capture 3 prescaler */
#define tim_cc3s (3 << 0) /* Capture/Compare 3 selection */
  /* Alternate */
#define tim_oc4ce (1 << 15) /* Output compare 4 clear enable */
#define tim_oc4m_3 (1 << 24) /* Output compare 4 mode, bit 3. */
#define tim_oc4m_0_2 (7 << 12) /* Output compare 4 mode, bits 0-2 */
#define tim_oc4pe (1 << 11) /* Output compare 4 preload enable */
#define tim_oc4fe (1 << 10) /* Output compare 4 fast enable */
#define tim_cc4s (3 << 8) /* Capture/Compare 4 selection */
#define tim_oc3ce (1 << 7) /* Output compare 3 clear enable */
#define tim_oc3m_3 (1 << 16) /* Output compare 3 mode, bit 3. */
#define tim_oc3m_0_2 (7 << 4) /* Output compare 3 mode, bits 0-2. */
#define tim_oc3pe (1 << 3) /* Output compare 3 preload enable */
#define tim_oc3fe (1 << 2) /* Output compare 3 fast enable */
#define tim_cc3s (3 << 0) /* Capture/Compare 3 selection */
  uint32_t ccm_r2; /* Capture/compare mode register 2. */

#define tim_cc4np (1 << 15) /* Capture/Compare 4 output Polarity. */
#define tim_cc4p (1 << 13) /* Capture/Compare 4 output Polarity. */
#define tim_cc4e (1 << 12) /* Capture/Compare 4 output enable. */
#define tim_cc3np (1 << 11) /* Capture/Compare 3 output Polarity. */
#define tim_cc3p (1 << 9) /* Capture/Compare 3 output Polarity. */
#define tim_cc3e (1 << 8) /* Capture/Compare 3 output enable. */
#define tim_cc2np (1 << 7) /* Capture/Compare 2 output Polarity. */
#define tim_cc2p (1 << 5) /* Capture/Compare 2 output Polarity. */
#define tim_cc2e (1 << 4) /* Capture/Compare 2 output enable. */
#define tim_cc1np (1 << 3) /* Capture/Compare 1 output Polarity. */
#define tim_cc1p (1 << 1) /* Capture/Compare 1 output Polarity. */
#define tim_cc1e (1 << 0) /* Capture/Compare 1 output enable. */
  uint32_t cce_r; /* Copture/compare enable register. */

  /* Mapping depends on UIFREMAP. */
#define tim_cnt (0x7fffffff << 0) /* Counter value */
#define tim_uifcpy (1 << 31)
  uint32_t cnt;

  uint16_t psc; /* Prescaler. */
  uint16_t reserved0;

  uint32_t ar_r; /* Auto reload register. */

  uint32_t reserved1;

  uint32_t cc_r1; /* Capture compare register 1. */
  uint32_t cc_r2; /* Capture compare register 2. */
  uint32_t cc_r3; /* Capture compare register 3. */
  uint32_t cc_r4; /* Capture compare register 4. */

  uint32_t reserved2;

#define tim_dbl (31 << 8) /* DMA burst length */
#define tim_dba (31 << 0) /* DMA base address */
  uint32_t dc_r; /* dma control register. */

  uint16_t dma_r;     /* dma address for full transfer. */
  uint16_t reserved3; /* dma address for full transfer. */

  union {
#define tim_ti4_rmp (3 << 2) /* Input Capture 4 remap */
#define tim_etr_rmp (1 << 1) /* External trigger remap */
#define tim_itr1_rmp (1 << 0) /* Internal trigger 1 remap */
    uint32_t tim2_o_r1;
#define tim_ti1_rmp (3 << 0)
    uint32_t tim3_o_r1;
  };

  uint32_t reserved4[3];

#define tim_etrsel (7 << 14) /* ETR source selection */
  union {
    uint32_t tim2_o_r2;
    uint32_t tim3_o_r2;
  };
} tim_regs_t;

static_assert(offsetof(tim_regs_t, ar_r) == 0x2c, "Offset check failed");
static_assert(offsetof(tim_regs_t, dc_r) == 0x48, "Offset check failed");
static_assert(offsetof(tim_regs_t, tim3_o_r2) == 0x60, "Offset check failed");

#endif /* ARCH_STM32L4XXX_PERIPHERALS_TIM_H_ */

#ifndef CORE_SYSTEM_H_
#define CORE_SYSTEM_H_

#include <stdint.h>
#include "kern/common.h"

typedef __IO struct {
  uint32_t actl_r; /* Auxiliary Control Register, ACTLR on page 4-5 */

  uint32_t reserved0;

#define scb_enable    (1 << 0)
#define scb_tickint   (1 << 1)
#define scb_clksource (1 << 2)
#define scb_countflag (1 << 16)
  uint32_t stcs_r; /* SysTick Control and Status Register */

  uint32_t strv_r; /* SysTick Reload Value Register */
  uint32_t stcv_r; /* SysTick Current Value Register */
  uint32_t stc_r;  /* SysTick Calibration Value Register */

  uint8_t reserved1[3296];

  uint32_t cpuid; /* CPUID Base Register, CPUID on page 4-5 */
  uint32_t ics_r; /* RO 0x00000000 Interrupt Control and State Register */
  uint32_t vto_r; /* Vector Table Offset Register */
  uint32_t airc_r; /* Application Interrupt and Reset Control Register */
  uint32_t sc_r; /* System Control Register */
  uint32_t cc_r; /* Configuration and Control Register. */
  uint32_t shp_r1; /* System Handler Priority Register 1 */
  uint32_t shp_r2; /* System Handler Priority Register 2 */
  uint32_t shp_r3; /* System Handler Priority Register 3 */
  uint32_t shcs_r; /* System Handler Control and State Register */
  uint32_t cfs_r; /* Configurable Fault Status Registers */
  uint32_t hfs_r; /* HardFault Status register */
  uint32_t dfs_r; /* Debug Fault Status Register */
  uint32_t mmfa_r; /* MemManage Address Registerb */
  uint32_t bfa_r; /* BusFault Address Registerb */
  uint32_t afs_r; /* Auxiliary Fault Status Register, AFSR on page 4-6 */
  uint32_t id_pf_r0; /* Processor Feature Register 0 */
  uint32_t id_pf_r1; /* Processor Feature Register 1 */
  uint32_t id_df_r0; /* Debug Features Register 0 */
  uint32_t id_af_r0; /* Auxiliary Features Register 0 */
  uint32_t id_mmf_r0; /* Memory Model Feature Register 0 */
  uint32_t id_mmf_r1; /* 0x00000000 Memory Model Feature Register 1 */
  uint32_t id_mmf_r2; /* Memory Model Feature Register 2 */
  uint32_t id_mmf_r3; /* Memory Model Feature Register 3 */
  uint32_t id_isa_r0; /* Instruction Set Attributes Register 0 */
  uint32_t id_isa_r1; /* Instruction Set Attributes Register 1 */
  uint32_t id_isa_r2; /* Instruction Set Attributes Register 2 */
  uint32_t id_isa_r3; /* Instruction Set Attributes Register 3 */
  uint32_t id_isa_r4; /* Instruction Set Attributes Register 4 */
  
  uint8_t reserved2[20];

  uint32_t cpac_r; /* Coprocessor Access Control Register */

  uint8_t reserved3[372];

  uint32_t sti_r; /* Software Triggered Interrupt Register */
} system_control_block_t;

#define ARM_SYSCFG_BASE 0xE000E008
#define CHECK_OFFSET(member, expected) \
  static_assert(ARM_SYSCFG_BASE + offsetof(system_control_block_t, member) == expected, \
      "Offset check failed")

CHECK_OFFSET(stcs_r, 0xE000E010);
CHECK_OFFSET(cpuid, 0xE000ED00);
CHECK_OFFSET(cpac_r, 0xE000ED88);
CHECK_OFFSET(id_mmf_r3, 0xE000ED5C);
CHECK_OFFSET(sti_r,  0xE000EF00);

#define SCB (*(system_control_block_t*)SYSTEM_CONFIG_BLOCK_BASE)

#endif

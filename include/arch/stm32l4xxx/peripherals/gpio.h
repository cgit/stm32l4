#ifndef CORE_GPIO_H__
#define CORE_GPIO_H__

#include "kern/common.h"
#include "arch/stm32l4xxx/peripherals/rcc.h"

#include <stdint.h>

/*
 * Structure defining the layout of the layout of the GPIO registers on the
 * stm32l432 development board.
 */
typedef struct GPIO_PORT_STR {
  /* Mode of each GPIO pin for this GPIO port. */
#define gpio_mode_n(off) (3 << ((off) * 2))
  __IO uint32_t mode_r;   /* Mode register */
 
  /* Output type for each gpio pin in this port. */
#define gpio_otype_n(off) (1 << (off))
  __IO uint32_t otype_r;

  /* GPIO port output speed. */
#define gpio_ospeed_n(off) (3 << ((off) * 2))
  __IO uint32_t ospeed_r;

  /* GPIO port pull-up/pull-down register */
#define gpio_pupd_n(off) (3 << ((off) * 2))
  __IO uint32_t pupd_r;

  /* GPIO port input data register. */
#define gpio_idr_n(off) (1 << (off))
  __IO uint32_t id_r;

  /* GPIO port output data register. */
#define gpio_odr_n(off) (1 << (off))
  __IO uint32_t od_r;

  /* GPIO port bit set/reset register. */
#define gpio_bs_n(off) (1 << (off))
#define gpio_br_n(off) (1 << (off))
  __IO uint32_t bsr_r;

  /* GPIO port configuration lock register. */
#define gpio_lck_n(off) (1 << (off))
#define gpio_lckk   (1 << 16)
  __IO uint32_t lck_r;

  /* Alternate function low-register. */
#define gpio_afsel_n(off) (0xf << ((off) * 4))
  __IO uint32_t af_rl;
  /* Alternate function high-register. */
  __IO uint32_t af_rh;

  /* GPIO port bit register. */
#define gpio_br_n(off) (1 << (off))
  __IO uint32_t br_r;

  /* Analog switch control register. */
#define gpio_asc_n(off) (1 << (off))
  __IO uint32_t asc_r;
} PACKED gpio_port_config_t;

static_assert(
    offsetof(gpio_port_config_t, asc_r) == 0x2C, "Offset check failed");

#endif

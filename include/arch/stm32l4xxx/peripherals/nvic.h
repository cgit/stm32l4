#ifndef NVIC_H_
#define NVIC_H_

#include "arch.h"
#include "kern/common.h"

typedef __IO struct {
#define nvic_intlinesnum (0x0F << 0)
  uint32_t ict_r; /* Interrupt control type register. */

  uint8_t reserved0[0xF8];

  uint32_t ise_r[8];

  uint8_t reserved1[0x60];

  uint32_t ice_r[8];

  uint8_t reserved2[0x60];

  uint32_t isp_r[8];

  uint8_t reserved3[0x60];

  uint32_t icp_r[8];

  uint8_t reserved4[0x60];

  uint32_t iab_r[8];
  
  uint8_t reserved5[0xE0];

  uint32_t ip_r[60];
} nvic_t;

static_assert(offsetof(nvic_t, ise_r) == 0x00FC, "Offset check failed");
static_assert(offsetof(nvic_t, ice_r) == 0x017C, "Offset check failed");
static_assert(offsetof(nvic_t, isp_r) == 0x01FC, "Offset check failed");
static_assert(offsetof(nvic_t, icp_r) == 0x027C, "Offset check failed");
static_assert(offsetof(nvic_t, iab_r) == 0x02FC, "Offset check failed");
static_assert(offsetof(nvic_t,  ip_r) == 0x03FC, "Offset check failed");

#define NVIC (* (nvic_t*) NVIC_BASE)


#endif /* NVIC_H_ */

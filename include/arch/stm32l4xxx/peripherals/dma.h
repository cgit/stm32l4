#ifndef CORE_DMA_H_
#define CORE_DMA_H_

/*
 * Header file for definining the DMA (Direct Memory Access).
 *
 * A DMA is used to perform data transfers between segments of memory
 * or between memory and peripherals.
 *
 * There are 2 DMA's on the chip. Each with 7 channels.
 */

#include "kern/common.h"
#include <arch.h>
#include <stdint.h>

#define DMA1 (* (dma_t*) DMA1_BASE)
#define DMA2 (* (dma_t*) DMA2_BASE)

typedef enum {
  DMA_SIZE_8_BITS = 0,
  DMA_SIZE_16_BITS = 1,
  DMA_SIZE_32_BITS = 2,
} dma_size_t;

typedef enum {
  DMA_PRIORITY_LEVEL_LOW = 0,
  DMA_PRIORITY_LEVEL_MEDIUM = 1,
  DMA_PRIORITY_LEVEL_HIGH = 2,
  DMA_PRIORITY_LEVEL_VERY_HIGH = 3
} dma_priority_level_t;

typedef enum {
  READ_FROM_PERIPHERAL = 0,
  READ_FROM_MEMORY = 1,
} dma_dir_t;

typedef struct {

#define dma_cc_en       (1 <<  0) // channel enable
#define dma_cc_tcie     (1 <<  1) // transfer complete interrupt enable
#define dma_cc_htie     (1 <<  2) // half transfer interrupt enable
#define dma_cc_teie     (1 <<  3) // transfer error interrupt enable
#define dma_cc_dir      (1 <<  4) // data transfer direction
#define dma_cc_circ     (1 <<  5) // circular mode
#define dma_cc_pinc     (1 <<  6) // peripheral increment mode
#define dma_cc_minc     (1 <<  7) // memory increment mode
#define dma_cc_psize    (3 <<  8) // Peripheral size
#define dma_cc_msize    (3 << 10) // Memory size
#define dma_cc_pl       (3 << 12) // Priority level
#define dma_cc_mem2mem  (1 << 14) // Memory to memory mode

  __IO uint32_t cc_r;

  /* Number of data to transfer. Can only store a short. */
  __IO uint32_t cndt_r;

  /* DMA channel peripheral address register.
   * Defines a memory address if mem2mem is set. */
  __IO uint32_t cpa_r;

  /* DMA channel memory address register.
   * Defines another perpipheral address if peripheral-periphal mode is set. */
  __IO uint32_t cma_r;

  __IO uint32_t reserved;
} dma_channel_config_t;

typedef struct {
  // DMA Interrupt status register.
#define dma_gif1  (1 <<  0) // global interrupt flag for channel 1
#define dma_tcif1 (1 <<  1) // transfer complete (TC) flag for channel 1
#define dma_htif1 (1 <<  2) // half transfer (HT) flag for channel 1
#define dma_teif1 (1 <<  3) // transfer error (TE) flag for channel 1
#define dma_gif2  (1 <<  4) // global interrupt flag for channel 2
#define dma_tcif2 (1 <<  5) // transfer complete (TC) flag for channel 2
#define dma_htif2 (1 <<  6) // half transfer (HT) flag for channel 2
#define dma_teif2 (1 <<  7) // transfer error (TE) flag for channel 2
#define dma_gif3  (1 <<  8) // global interrupt flag for channel 3
#define dma_tcif3 (1 <<  9) // transfer complete (TC) flag for channel 3
#define dma_htif3 (1 << 10) // half transfer (HT) flag for channel 3
#define dma_teif3 (1 << 11) // transfer error (TE) flag for channel 3
#define dma_gif4  (1 << 12) // global interrupt flag for channel 4
#define dma_tcif4 (1 << 13) // transfer complete (TC) flag for channel 4
#define dma_htif4 (1 << 14) // half transfer (HT) flag for channel 4
#define dma_teif4 (1 << 15) // transfer error (TE) flag for channel 4
#define dma_gif5  (1 << 16) // global interrupt flag for channel 5
#define dma_tcif5 (1 << 17) // transfer complete (TC) flag for channel 5
#define dma_htif5 (1 << 18) // half transfer (HT) flag for channel 5
#define dma_teif5 (1 << 19) // transfer error (TE) flag for channel 5
#define dma_gif6  (1 << 20) // global interrupt flag for channel 6
#define dma_tcif6 (1 << 21) // transfer complete (TC) flag for channel 6
#define dma_htif6 (1 << 22) // half transfer (HT) flag for channel 6
#define dma_teif6 (1 << 23) // transfer error (TE) flag for channel 6
#define dma_gif7  (1 << 24) // global interrupt flag for channel 7
#define dma_tcif7 (1 << 25) // transfer complete (TC) flag for channel 7
#define dma_htif7 (1 << 26) // half transfer (HT) flag for channel 7
#define dma_teif7 (1 << 27) // transfer error (TE) flag for channel 7
  __IO uint32_t is_r;

  // DMA Interrupt flag clear register
#define dma_cgif1  (1 <<  0) // global interrupt flag clear for channel 1
#define dma_ctcif1 (1 <<  1) // transfer complete flag clear for channel 1
#define dma_chtif1 (1 <<  2) // half transfer flag clear for channel 1
#define dma_cteif1 (1 <<  3) // transfer error flag clear for channel 1
#define dma_cgif2  (1 <<  4) // global interrupt flag clear for channel 2
#define dma_ctcif2 (1 <<  5) // transfer complete flag clear for channel 2
#define dma_chtif2 (1 <<  6) // half transfer flag clear for channel 2
#define dma_cteif2 (1 <<  7) // transfer error flag clear for channel 2
#define dma_cgif3  (1 <<  8) // global interrupt flag clear for channel 3
#define dma_ctcif3 (1 <<  9) // transfer complete flag clear for channel 3
#define dma_chtif3 (1 << 10) // half transfer flag clear for channel 3
#define dma_cteif3 (1 << 11) // transfer error flag clear for channel 3
#define dma_cgif4  (1 << 12) // global interrupt flag clear for channel 4
#define dma_ctcif4 (1 << 13) // transfer complete flag clear for channel 4
#define dma_chtif4 (1 << 14) // half transfer flag clear for channel 4
#define dma_cteif4 (1 << 15) // transfer error flag clear for channel 4
#define dma_cgif5  (1 << 16) // global interrupt flag clear for channel 5
#define dma_ctcif5 (1 << 17) // transfer complete flag clear for channel 5
#define dma_chtif5 (1 << 18) // half transfer flag clear for channel 5
#define dma_cteif5 (1 << 19) // transfer error flag clear for channel 5
#define dma_cgif6  (1 << 20) // global interrupt flag clear for channel 6
#define dma_ctcif6 (1 << 21) // transfer complete flag clear for channel 6
#define dma_chtif6 (1 << 22) // half transfer flag clear for channel 6
#define dma_cteif6 (1 << 23) // transfer error flag clear for channel 6
#define dma_cgif7  (1 << 24) // global interrupt flag clear for channel 7
#define dma_ctcif7 (1 << 25) // transfer complete flag clear for channel 7
#define dma_chtif7 (1 << 26) // half transfer flag clear for channel 7
#define dma_cteif7 (1 << 27) // transfer error flag clear for channel 7
  __IO uint32_t ifc_r;

  dma_channel_config_t channel_config[7];

  __IO uint32_t reserved[5];

  /* DMA channel selection register. */
#define dma_c1s (0xF <<  0) // DMA channel 1 selection.
#define dma_c2s (0xF <<  4) // DMA channel 2 selection.
#define dma_c3s (0xF <<  8) // DMA channel 3 selection.
#define dma_c4s (0xF << 12) // DMA channel 4 selection.
#define dma_c5s (0xF << 16) // DMA channel 5 selection.
#define dma_c6s (0xF << 20) // DMA channel 6 selection.
#define dma_c7s (0xF << 24) // DMA channel 7 selection.
  __IO uint32_t csel_r;
} dma_t;

static_assert(offsetof(dma_t, csel_r) == 0xA8, "Offset check failed.");

#endif /* CORE_DMA_H_ */

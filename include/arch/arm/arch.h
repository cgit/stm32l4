#ifndef ARCH_H_
#define ARCH_H_

#ifndef ARCH_STM32L4
#define ARCH_STM32L4
#endif

#define CORTEX_M4

#define enable_all_interrupts() \
  asm volatile(" cpsie i ")

#define disable_all_interrupts() \
  asm volatile(" cpsid i ")

#define __isb() \
  asm volatile(" isb ")

#define __dsb() \
  asm volatile(" dsb ")


#define DMA1_BASE (0x40020000)
#define DMA2_BASE (0x40020400)

#define USART1_BASE (0x40013800)
#define USART2_BASE (0x40004400)

#define GPIOA_BASE (0x48000000)
#define GPIOB_BASE (0x48000400)
#define GPIOC_BASE (0x48000800)
#define GPIOH_BASE (0x48001C00)

#define SRAM1_BASE (0x20000000)
#define SRAM2_BASE (0x2000C000)

#define SYSTEM_CONFIG_BLOCK_BASE (0xE000E008)
#define NVIC_BASE (0xE000E004)
#define RCC_BASE (0x40021000)

#define SPI1_BASE (0x40013000)
#define SPI3_BASE (0x40003C00)

#define STACK_TOP (0x2000c000)
#define MPU_BASE (0xE000ED90)

#define SYSCFG_BASE (0x40010000)
#define EXTI_BASE (0x40010400)

#define TIM2_BASE (0x40000000)

#include <stdint.h>
#ifndef DRY_RUN
_Static_assert(sizeof(void*) == sizeof(uint32_t), "Pointers must be 32 bits");
#endif

extern uint32_t DATA_SEGMENT_START;
extern uint32_t DATA_SEGMENT_STOP;

extern unsigned char HEAP_START;
extern unsigned char HEAP_STOP;

#endif /* ARCH_H_ */

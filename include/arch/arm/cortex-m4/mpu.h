#ifndef ARCH_ARM_CORTEX_M4_MPU_H_
#define ARCH_ARM_CORTEX_M4_MPU_H_

#include "arch.h"

typedef volatile struct {
  volatile uint32_t type_r;
#define mpu_en (1 << 0)
  volatile uint32_t ctrl_r;
  volatile uint32_t rn_r;

  /**
   * On the ARM Cortex-M4 processor, the
   */
  volatile union {
#define mpu_size (0x1F << 1)
#define mpu_srd (0xFF << 8)
#define mpu_b (1 << 16)
#define mpu_c (1 << 17)
#define mpu_s (1 << 18)
#define mpu_tex (7 << 19)
#define mpu_ap (3 << 24)
#define mpu_xn (1 << 28)

#define mpu_valid (1 << 4)
#define mpu_region (0xF << 0)
    struct {
      uint32_t rba_r;
      uint32_t ras_r;
    };
    struct {
      uint32_t rba_r;
      uint32_t ras_r;
    } aliased[4];
  };
} mpu_t;

#define MPU (*((mpu_t*)(MPU_BASE)))

#endif /* ARCH_ARM_CORTEX_M4_MPU_H_ */

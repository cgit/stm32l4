#ifndef KERN_SYSCALL_H_
#define KERN_SYSCALL_H_

#define SYSCALL(id, fn, kernfn, argt) void kernfn(argt arg);
#include "kern/syscall/syscall_tbl.inc"
#undef SYSCALL

#endif

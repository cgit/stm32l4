#ifndef KERN_MPU_MPU_MANAGER_H_
#define KERN_MPU_MPU_MANAGER_H_

#include "kern/common.h"

typedef enum {
  REGION_SIZE_32 = 4,
  REGION_SIZE_64 = 5,
  REGION_SIZE_128 = 6,
  REGION_SIZE_256 = 7,
  REGION_SIZE_512 = 8,
  REGION_SIZE_1Kb = 9,
  REGION_SIZE_2Kb = 10,
  REGION_SIZE_4Kb = 11,
  REGION_SIZE_8Kb = 12,
  REGION_SIZE_16Kb = 13,
  REGION_SIZE_32Kb = 14,
  REGION_SIZE_64Kb = 15,
  REGION_SIZE_128Kb = 16,
  REGION_SIZE_256Kb = 17,
  REGION_SIZE_512Kb = 18,
  REGION_SIZE_1Mb = 19,
  REGION_SIZE_2Mb = 20,
  REGION_SIZE_4Mb = 21,
  REGION_SIZE_8Mb = 22,
  REGION_SIZE_16Mb = 23,
  REGION_SIZE_32Mb = 24,
  REGION_SIZE_64Mb = 25,
  REGION_SIZE_128Mb = 26,
  REGION_SIZE_256Mb = 27,
  REGION_SIZE_512Mb = 28,
  REGION_SIZE_1Gb = 29,
  REGION_SIZE_2Gb = 30,
  REGION_SIZE_4Gb = 31,
} region_size_t;

#define region_size_mask(region_size) ((1 << (region_size + 1)) - 1)

typedef enum {
  /* Neither Privileged nor non-Privileged code cannnot access this region */
  ACCESS_PERMS_NO_ACCESS = 0,

  /* Only privileged users can access this memory. */
  ACCESS_PERMS_ONLY_PRIV = 1,

  /* Privileged code can access fully, but non-privilege only has Read-only
     access.*/
  ACCESS_NON_PERMS_RO = 2,

  /* Both Privileged and non-Privileged code can access this region fully. */
  ACCESS_PERMS_FULL = 3,

  /* Only privileged users can access this memory, and only as Read-only. */
  ACCESS_PERMS_ONLY_PRIV_RO = 5,

  /* Both privileged and non-privileged users can access this memory, but only
     as Read-only.*/
  ACCESS_PERMS_BOTH_RO = 6,
} access_perms_t;

typedef struct {
  region_size_t size;
  uint8_t subregion_disable;

  struct {
    bool enable : 1;
    bool sharable : 1;
    bool cacheable : 1;
    bool bufferable : 1;
    bool executable : 1;
    uint8_t tex : 3;
  };

  access_perms_t perms;
  void* region;
} memory_region_opts_t;

void mpu_set_enabled(bool enabled);

/* Configures a memory region. The region number must be on the interval [0,8).
 */
void mpu_configure_region(int region_number, memory_region_opts_t* opts);

typedef enum {
  PRIVILEGED,
  NOT_PRIVILEGED,
} privilege_t;


/** Configure a peripheral region with default peripheral attributes. */
void configure_peripheral_region(
    void* peripheral_base,
    region_size_t region_size,
    privilege_t priv);

/** Configure a flash region with default flash attributes. */
void configure_flash_region(
    void* flash_base,
    region_size_t region_size,
    privilege_t priv);

/** Configure a ram region with default ram attributes. */
void configure_ram_region(
    void* ram_base,
    region_size_t region_size,
    privilege_t priv);

#endif /* KERN_MPU_MPU_MANAGER_H_ */

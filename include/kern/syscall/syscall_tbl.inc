/*
 * Defines a table of SYSCALLS. The syscalls need to be void functions that
 * a single 32-bit integer as their argument.
 */
SYSCALL(25, logs, kern_logs, const char*)

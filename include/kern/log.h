#ifndef LOG_H_
#define LOG_H_

#include <stdarg.h>

/*
 * Defines logging capabilities. This logging unit will enable logging on
 * the systems main USART output.
 */

/** Similar to fprintf, but with a stripped-down format-string DSL. */
void klogf(const char* fmt, ...);

/** Like klogf, but with a va_list argument. */
void kvlogf(const char* fmt, va_list args);

/** Log the following message as an error. */
void kerr_logf(const char* fmt, ...);

/** Like klogf, but with a va_list argument. */
void kerr_vlogf(const char* fmt, va_list args);

void initialize_logging();

#endif

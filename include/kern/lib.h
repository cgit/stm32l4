#ifndef LIB_H_
#define LIB_H_

#include <stdint.h>

void hexify(uint32_t v, char* into);

void decimalify(int v, char* into);

#endif

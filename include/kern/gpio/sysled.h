/*
 * Headers for interacting and managing the system LED.
 */
#ifndef SYSLED_H_
#define SYSLED_H_

#include "kern/gpio/gpio_manager.h"

gpio_reserved_pin_t get_sysled();

#endif

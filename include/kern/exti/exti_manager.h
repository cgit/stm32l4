#ifndef _KERN_EXTI_EXTI_MANAGER_H_
#define _KERN_EXTI_EXTI_MANAGER_H_

#include "kern/common.h"
#include "kern/gpio/gpio_manager.h"
#include "shared/linked_list.h"

#define EXTI_ERROR_ALREADY_IN_USE 1

struct EXTI_HANDLE;


typedef struct {
  void (*fn)(struct EXTI_HANDLE, void*);
  void* closure;
} exti_callback_t;

LINKED_LIST_DECL(exti_callback_t);
typedef struct EXTI_HANDLE {
  uint8_t id;
  linked_list_t(exti_callback_t)* callbacks;
} exti_handle_t;

exti_handle_t* enable_exti_for_gpio(gpio_pin_t gpio_pin, int* ec);

exti_handle_t* exti_add_callback(exti_handle_t*, exti_callback_t* callback);

gpio_pin_t exti_gpio_pin(exti_handle_t* handle);



#endif /* _KERN_EXTI_EXTI_MANAGER_H_ */

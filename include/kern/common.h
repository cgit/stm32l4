#ifndef COMMON__H
#define COMMON__H

#include <assert.h>
#include <stddef.h>
#include <stdint.h>

typedef enum { ENDIANNESS_LITTLE, ENDIANNESS_BIG } endianness_t;

#define WEAK __attribute__((weak))
#define NORETURN __attribute__((noreturn))

#ifndef static_assert
#define static_assert(a, b)
#endif

/* Define __IO to be volatile if it's not already. */
#ifndef __IO
#define __IO volatile
#endif

#define CTZ(n) __builtin_ctz(n)

#define bool unsigned int
#ifndef __cplusplus
#define true 1
#define false 0
#endif

#define PACKED __attribute__((packed))
#define BIT(n) (1 << (n))

#define RESERVED_CONCAT_IMPL(x, y) x##y
#define RESERVED_MACRO_CONCAT(x, y) RESERVED_CONCAT_IMPL(x, y)
#define RESERVED(n) bits_t RESERVED_MACRO_CONCAT(_r, __COUNTER__) : n

#define RESERVE(type) __IO type RESERVED_MACRO_CONCAT(_r, __COUNTER__)

#define ptr2reg(ptr) ((uint32_t)(ptrdiff_t)(ptr))

typedef __IO uint32_t bits_t;
typedef uint16_t msize_t;

#define regset(reg, mask, val) ((reg) = ((reg) & ~mask) | (val << CTZ(mask)))

#define regget(reg, mask) (((reg)&mask) >> (CTZ(mask)))

#endif /* COMMON_H */

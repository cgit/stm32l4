#ifndef INIT_H_
#define INIT_H_

/** Globals annotated with _no_init will not be set during init1 bootup
 * where the data segement is loaded from flash and the bss segment is
 * cleared.
 *
 * This is useful for routines that run in the init0 boot procedure
 * that need persistent globals.
 *
 * Note that initializing a global annotated with _no_init will have
 * no effect as the variable will remain uninitialized until explicitly
 * set by by the program.
 */
#define _no_init \
  __attribute((__section__(".noinit")))

#define init0 \
  static void init0fn(); \
  static __attribute((__section__(".init0"))) \
  __attribute((__used__)) \
  void(*init0_ptr)() = init0fn; \
  static void init0fn
#define init1 \
  static void init1fn(); \
  static __attribute((__section__(".init1"))) \
  __attribute((__used__)) \
  void(*init1_ptr)() = init1fn; \
  static void init1fn
#define init2 \
  static void init2fn(); \
  static __attribute((__section__(".init2"))) \
  __attribute((__used__)) \
  void(*init2_ptr)() = init2fn; \
  static void init2fn
#define init3 \
  static void init3fn(); \
  static __attribute((__section__(".init3"))) \
  __attribute((__used__)) \
  void(*init3_ptr)() = init3fn; \
  static void init3fn
#define init4 \
  static void init4fn(); \
  static __attribute((__section__(".init4"))) \
  __attribute((__used__)) \
  void(*init4_ptr)() = init4fn; \
  static void init4fn
#define init5 \
  static void init5fn(); \
  static __attribute((__section__(".init5"))) \
  __attribute((__used__)) \
  void(*init5_ptr)() = init5fn; \
  static void init5fn
#define init6 \
  static void init6fn(); \
  static __attribute((__section__(".init6"))) \
  __attribute((__used__)) \
  void(*init6_ptr)() = init6fn; \
  static void init6fn
#define init7 \
  static void init7fn(); \
  static __attribute((__section__(".init7"))) \
  __attribute((__used__)) \
  void(*init7_ptr)() = init7fn; \
  static void init7fn

#define entry_point(fn) \
  static __attribute((__section__(".entry_point"))) __attribute((__used__)) \
  struct { \
    uint32_t _unused; \
    uint32_t entry_point; \
  } c = { 0x2000c000, (uint32_t) &fn }

typedef enum {
  INIT_LEVEL_0,
  INIT_LEVEL_1,
  INIT_LEVEL_2,
  INIT_LEVEL_3,
  INIT_LEVEL_4,
  INIT_LEVEL_5,
  INIT_LEVEL_6,
  INIT_LEVEL_7,
  INIT_LEVEL_MAIN,
} init_level_t;

init_level_t get_system_init_level();

#endif /* INIT_H_ */

#ifndef KERN_SYSTICK_SYSTICK_MANAGER_H_
#define KERN_SYSTICK_SYSTICK_MANAGER_H_

#include "kern/common.h"

/*
 * Enable the systick timer.
 *
 * The systick counter counts down from the counter. When the counter
 * hits 0, an interrupt is and the callbacks are fired.
 */
void enable_systick(uint32_t systick_counter);

/*
 * Disables the systick timer.
 */
void disable_systick();

/*
 * Add a callback to the systick system. These callbacks will
 * be callled when the interrupt is raised.
 */
void systick_add_callback(void(*callback)(void*), void* arg);

#endif /* KERN_SYSTICK_SYSTICK_MANAGER_H_ */

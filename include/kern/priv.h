#ifndef _KERN_PRIV_H_
#define _KERN_PRIV_H_

#include "kern/common.h"

uint32_t get_control_register();

void set_control_register(uint32_t reg); 

/* Enters user mode from privilieged mode. */
void enter_user_mode();

void jump_to_user_mode();


#endif /* _KERN_PRIV_H_ */

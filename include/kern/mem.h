#ifndef MEM_H_
#define MEM_H_

#include "arch.h"
#include <stddef.h>

void kalloc_init();

/* allocates memory on the head, which is stored in sram2 */
void* kalloc(size_t n);

/* Frees the memory allocated by kalloc. */
void kfree(void* mem);

#ifdef FOR_TESTING

void* debug_kalloc_get_next_ptr(void* ptr);

void* debug_kalloc_get_prev_ptr(void* ptr);

int debug_kalloc_assert_consistency(char* error, size_t len);

void debug_print_blocks();

int debug_is_heap_empty();

#endif


#endif

#ifndef KERN_PANIC_H_
#define KERN_PANIC_H_

_Noreturn void panic(const char* fmt, ...);

#endif

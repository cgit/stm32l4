#ifndef USER_SYSCALL_H_
#define USER_SYSCALL_H_

/* This defines the full set of system calls accessible from usermode. */

#define SYSCALL(id, fn, kernfn, argt) void fn(argt arg);
#include "kern/syscall/syscall_tbl.inc"
#undef SYSCALL

#endif

#ifndef _SHARED_MATH_H_
#define _SHARED_MATH_H_

#include "kern/common.h"

/* Returns round(sin(2πn / 256) * 127.5 + 127.5). */
uint8_t byte_sin(uint8_t n);

static inline uint8_t byte_scale(uint8_t n, uint8_t sc) {
  return n * sc / 255;
}

#define min(a, b) (a) < (b) ? (a) : (b)
#define max(a, b) (a) > (b) ? (a) : (b)

/* returns (in / 256)^n * 256. */
uint8_t amp(uint8_t in, uint8_t n);

#endif /* _SHARED_MATH_H_ */

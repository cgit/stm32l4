#ifndef _SHARED_STDMACRO_H_
#define _SHARED_STDMACRO_H_

#define CONCAT_(a, b) a ## b
#define CONCAT(a, b) CONCAT_(a, b)

#endif /* _SHARED_STDMACRO_H_ */

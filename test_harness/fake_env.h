#ifndef FAKE_ENV_H_
#define FAKE_ENV_H_

/* Functions which wil lazily load fake chunks of memory
 * corresponding to the*/
void* load_fake_ahb1__();
void* load_fake_ahb2__();
void* load_fake_apb1__();
void* load_fake_apb2__();
void* load_fake_sram1__();
void* load_fake_sram2__();
void* load_fake_scb__();
void* load_fake_nvic__();
void* load_fake_rcc__();
void* load_fake_spi1__();
void* load_fake_spi3__();
void* load_fake_mpu__();
void* load_fake_syscfg__();
void* load_fake_exti__();
void* load_fake_tim2__();

void wipeout_fake_env();

#endif /* FAKE_ENV_H_ */

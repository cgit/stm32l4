#include "fake_env.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

struct fakeenv_memseg {
  const char* name;
  void* segment;
};

#define DEFINE_MEMORY_SEGMENT(seg, start_addr, end_addr)            \
  static __attribute((                                              \
      __section__("fakeenv"))) struct fakeenv_memseg fake_##seg = { \
      .name = #seg,                                                 \
      .segment = NULL,                                              \
  };                                                                \
  void* load_fake_##seg##__()                                       \
  {                                                                 \
    if (fake_##seg.segment == NULL) {                               \
      fake_##seg.segment = malloc((end_addr) - (start_addr));       \
      assert(fake_##seg.segment != NULL);                           \
    }                                                               \
    return fake_##seg.segment;                                      \
  }

extern struct fakeenv_memseg __start_fakeenv;
extern struct fakeenv_memseg __stop_fakeenv;

void wipeout_fake_env()
{
  for (struct fakeenv_memseg* iter = &__start_fakeenv; iter < &__stop_fakeenv;
       ++iter) {
    free(iter->segment);
    iter->segment = NULL;
  }
}

/* Reset and clock control. */
DEFINE_MEMORY_SEGMENT(rcc, 0x40021000, 0x400210A0)

/* Peripheral buses */
DEFINE_MEMORY_SEGMENT(apb1, 0x40000000, 0x40010000)
DEFINE_MEMORY_SEGMENT(apb2, 0x40010000, 0x40020000)
DEFINE_MEMORY_SEGMENT(ahb1, 0x40020000, 0x40024400)
DEFINE_MEMORY_SEGMENT(ahb2, 0x48000000, 0x50060C00)

/* System Control Block */
DEFINE_MEMORY_SEGMENT(scb, 0xE000E008, 0xE000EF04)

/* Nested Vector Interrupt Controller (NVIC) */
/* Note that this memory space acutally overlaps with the SCB, but
 * they are functionally distinct entitites and such are modeled as
 * separate structures in memeory. */
DEFINE_MEMORY_SEGMENT(nvic, 0xE000E004, 0xE000E4F0)

/* SRAM */
DEFINE_MEMORY_SEGMENT(sram1, 0x20000000, 0x2000C000)
DEFINE_MEMORY_SEGMENT(sram2, 0x2000C000, 0x20018000)
DEFINE_MEMORY_SEGMENT(mpu, 0xE000ED90, 0xE000EDA4)

/* Serial Peripheral Interface */
DEFINE_MEMORY_SEGMENT(spi1, 0x40013000, 0x400133FF)
DEFINE_MEMORY_SEGMENT(spi3, 0x40003C00, 0x40003FFF)

DEFINE_MEMORY_SEGMENT(syscfg, 0x40010000, 0x4001002F)
DEFINE_MEMORY_SEGMENT(exti, 0x40010400, 0x40010800)

DEFINE_MEMORY_SEGMENT(tim2, 0x40000000, 0x400003FF)
